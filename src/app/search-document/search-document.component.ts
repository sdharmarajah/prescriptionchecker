import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Global } from '../global';
import { Prescription } from '../prescription';
import { PrescriptionDetails } from '../prescription-details';

import { PrescriptionService } from '../prescription.service';
import { SpinnerVisibilityService } from 'ng-http-loader';

import * as $ from 'jquery';

@Component({
  selector: 'app-search-document',
  templateUrl: './search-document.component.html',
  styleUrls: ['./search-document.component.css']
})
export class SearchDocumentComponent implements OnInit {

  prescription: Prescription;
  prescriptionDetails: PrescriptionDetails;
  disabledBtn: boolean;
  verifyBtn: boolean;
  prescriptionJson: any;

  @Input() status : Global;
  @Output() showCancelBtn: EventEmitter<any> = new EventEmitter<any>();

  constructor(private prescriptionResponse: PrescriptionService, private spinner: SpinnerVisibilityService) { }

  ngOnInit() {
    this.prescription = {
      'firstLetter' : "",
      'secondLetter' : "",
      'thirdLetter' : "",
      'referenceCode' : ""
    };
    this.status.disabledBtn = true;

    $(document).ready(function() {
      $('.doctor-code').focus();
    });

    this.verifyBtn = true;
  }

  nextSearchPatient(code: string): void {
    this.prescription = {
      'firstLetter' : '',
      'secondLetter' : '',
      'thirdLetter' :  '',
      'referenceCode' : code
    };
    this.status.status = false;
    this.showCancelBtn.emit(true);

    $(document).ready(function(){
        $(".firstLetter").focus();
        $('.letter').each(function(index, value) {
          var obj = $(this);
          if(typeof obj.value === "undefined" && index !== 0) {
            obj.attr('disabled', true);
          }
        });
    });
  }

  onKeyCode(event: any): void { // without type info
      if(event.target.value !== "") {
        this.status.disabledBtn = false;
      } else {
        this.status.disabledBtn = true;
      }
  }

  onEnter(code: string): void {
    this.prescription = {
      'firstLetter' : '',
      'secondLetter' : '',
      'thirdLetter' :  '',
      'referenceCode' : code
    };
    this.status.status = false;
    this.showCancelBtn.emit(true);

    $(document).ready(function(){
        $(".firstLetter").focus();
        $('.letter').each(function(index, value) {
          var obj = $(this);
          if(typeof obj.value === "undefined" && index !== 0) {
            obj.attr('disabled', true);
          }
        });


    });
  }

  onKey(event: any, index: any): void { // without type info
    if(event.key !== "Shift" && event.key !== "Tab") {
      if(event.target.value !== "") {
        if(this.checkBtn()) {
          $(".letter-"+ (index + 1)).attr('disabled', false);
          $(".letter-"+ (index + 1)).focus();
          if(!this.checkBtn()) {
            $('.verifyBtn').focus();
          }
        } else {
          this.verifyBtn = false;
          $('.verifyBtn').focus();
        }
      } else {
        this.verifyBtn = true;
        if(!this.checkBtn()) {
          $('.verifyBtn').focus();
        }
      }
    }
  }

  onEnterVerify(prescription: Prescription): void {
    if(!this.checkBtn()) {
      this.prescriptionJson = {};
      this.spinner.show();
      this.prescriptionDetails = {
        "reference_code": prescription.referenceCode, 
        "first_three":prescription.firstLetter + prescription.secondLetter + prescription.thirdLetter,
        "password": "test!"
      }
      this.prescriptionResponse.checkPrescription(this.prescriptionDetails)
        .subscribe(
            prescriptionJson => {
              this.prescriptionJson = prescriptionJson;
              this.status.btnName = (this.prescriptionJson.status === 'Valid') ? "Clear" : "Restart";
              this.status.status = true;
              this.status.page = true;
              this.spinner.hide();
            });
    }
  }

  checkBtn() : boolean {
    let hasError = [];
    $('.letter').each(function() {
      var obj = $(this);
      if(obj.val() === "")
        hasError.push(1);
      else
        hasError.push(0);
    });

    if(hasError.indexOf(1) !== -1) 
      return true;
    else
      return false;
  }

  verify(prescription: Prescription): void {
    this.prescriptionJson = {};
    this.spinner.show();
    this.prescriptionDetails = {
      "reference_code": prescription.referenceCode, 
      "first_three":prescription.firstLetter + prescription.secondLetter + prescription.thirdLetter,
      "password": "test!"
    }
    this.prescriptionResponse.checkPrescription(this.prescriptionDetails)
      .subscribe(
          prescriptionJson => {
            if(this.prescriptionJson.status === 'invalid') {
              prescriptionJson.reference_code = prescription.referenceCode;
            }
            this.prescriptionJson = prescriptionJson;
            this.status.btnName = (this.prescriptionJson.status === 'Valid') ? "Clear" : "Restart";
            this.status.status = true;
            this.status.page = true;
            this.spinner.hide();
          });
  }

}
