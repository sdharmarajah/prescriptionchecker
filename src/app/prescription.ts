export class Prescription {
    firstLetter: string;
    secondLetter: string;
    thirdLetter: string;
    referenceCode: string;
  }
  