import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionCheckerComponent } from './prescription-checker.component';

describe('PrescriptionCheckerComponent', () => {
  let component: PrescriptionCheckerComponent;
  let fixture: ComponentFixture<PrescriptionCheckerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescriptionCheckerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
