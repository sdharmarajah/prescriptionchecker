import { Component, OnInit } from '@angular/core';
import { Global } from '../global';

import * as $ from 'jquery';

@Component({
  selector: 'app-prescription-checker',
  templateUrl: './prescription-checker.component.html',
  styleUrls: ['./prescription-checker.component.css']
})
export class PrescriptionCheckerComponent implements OnInit {
  cancelBtn = false;

  defaultStatus: Global;

  constructor() {}

  ngOnInit() {
    this.defaultStatus = {
      status: true,
      page: false,
      disabledBtn: true,
      btnName: 'Back'
    }
  }

  public setCalcelBtnStatus(status: any):void {
    if(status) {
      this.cancelBtn = true;
    } else {
      this.cancelBtn = false;
    }
  }

  back(label: any): void {
    this.defaultStatus = {
      status: true,
      page: false,
      disabledBtn: true,
      btnName: 'Back'
    }
    this.cancelBtn = false;

    $(document).ready(function() {
      console.log(label);
      if(label == 'Back') {
        if($('.doctor-code').val() !== "") {
          $('.next-btn').attr('disabled', false);
        }
        $('.doctor-code').focus();
      } else {
        $('.next-btn').attr('disabled', true);
        $('.doctor-code').val('');
        $('.doctor-code').focus();
      }
    });
  }
}
