export class PrescriptionResponse {
    firstLetter: string;
    secondLetter: string;
    thirdLetter: string;
    referenceCode: string;
  }
  