import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { PrescriptionResponse } from './prescription-response';
import { PrescriptionDetails } from '../app/prescription-details';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {

  //private apiUrl = 'http://localhost/demo/';  // URL to web api
  private apiUrl = 'https://13.250.170.79/prescription';  // URL to web api

  constructor(
    private http: HttpClient
  ) { }

  getPrescriptionResponse (): Observable<PrescriptionResponse[]> {
    return this.http.get<PrescriptionResponse[]>(this.apiUrl)
      .pipe(
        catchError(this.handleError('getPrescriptionResponse', []))
      );
  }

  checkPrescription (prescriptionDetails: PrescriptionDetails): Observable<PrescriptionDetails> {
    return this.http.post<PrescriptionDetails>(this.apiUrl, prescriptionDetails, httpOptions)
      .pipe(
        catchError(this.handleError('addHero', prescriptionDetails))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
